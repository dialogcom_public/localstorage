function localStorageGet($ID, $FallbackValue = null) {
    const data = JSON.parse(localStorage.getItem($ID));
    return (data) ? data.val : $FallbackValue;
}

function localStorageSet($ID, $Value = null) {
    const data = JSON.stringify({val: $Value});
    localStorage.setItem($ID, data);
    return localStorageGet($ID);
}

module.exports = {
    localStorageGet: localStorageGet,
    localStorageSet: localStorageSet
};
